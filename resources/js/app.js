/**
 * First, we will load all of this project's Javascript utilities and other
 * dependencies. Then, we will be ready to develop a robust and powerful
 * application frontend using useful Laravel and JavaScript libraries.
 */

require('./bootstrap');
import Vue from 'vue';

import Routes from '@/js/routes.js';
import '~/plugins'
import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min.css'

import App from '@/js/views/App';

Vue.use(Vuetify);

export default new Vuetify({
    iconfont: 'md',
})
new Vue({
    router: Routes,
    vuetify: new Vuetify(),
    render: h => h(App),
}).$mount('#app');

